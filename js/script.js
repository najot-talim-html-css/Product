window.addEventListener("scroll", function () {
  shrink();
});

let navbar = document.getElementById("navbar");

function shrink() {
  if (scrollY > 100) {
    navbar.classList.add("navbar-shrink");
  } else {
    navbar.classList.remove("navbar-shrink");
  }
}


let indicator = document.querySelector(".scroll-indicator .progress");
let scrollHeight =
  document.documentElement.scrollHeight - document.documentElement.clientHeight;

window.addEventListener("scroll", scroll);

function scroll() {
  let scrollTop = document.documentElement.scrollTop;
  let scrolled = (scrollTop / scrollHeight) * 100;
  indicator.style.width = `${scrolled}%`;
}


// --- Scroll --- //
const scrollBtn = document.querySelector('.scrollToTop-btn')
window.addEventListener('scroll', () => {
  scrollBtn.classList.toggle('active', window.scrollY > 150)
})
scrollBtn.addEventListener('click', () => {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
})

window.addEventListener('scroll', () => {
  let reveals = document.querySelectorAll('.reveal')

  for(let i = 0; i< reveals.length; i++) {
    let windowHeight = window.innerHeight;
    let revealTop = reveals[i].getBoundingClientRect().top;
    let revealPoint = 90;

    if(revealTop < windowHeight - revealPoint) {
      reveals[i].classList.add('active')
    }
  }
})


document.addEventListener("contextmenu", (event) => event.preventDefault());

document.addEventListener("keydown", (event) => {
  if (event.keyCode === 123, "I" || (event.ctrlKey && event.altKey && event.shiftKey)) {
    event.preventDefault();
  }
});

AOS.init({
  duration: 3000,
  once: true,
});

$(".owl-carousel-1").owlCarousel({
  loop: true,
  nav: true,
  dots: true,
  margin: 20,
  responsiveClass: true,
  responsive: {
    0: {
      items: 1,
    },
    600: {
      items: 2,
    },
    1000: {
      items: 3,
    },
  },
});


let modeBtn = document.getElementById("dark-light");

modeBtn.addEventListener("click", function () {
  if (document.body.className != "light") {
    this.firstElementChild.src = "img/light.svg";
  } else {
    this.firstElementChild.src = "img/Dark.svg";
  }
  document.body.classList.toggle("light");
});


  